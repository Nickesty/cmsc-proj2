#include<stdio.h>
#include<stdlib.h>
	
struct ArrayList{
        int * array;
        int sizex;
        int sizey;
};

int * getPos(int x, int y, struct ArrayList a){
        int *ptr = a.array +x + y*a.sizex;
        return ptr;
}


int isMatching(char x, char y){//TODO When done with testing against old testcases, switch to rna
	if(x=='H' && y=='G')
		return 1;
	if(x=='G' && y=='H')
		return 1;
	if(x=='W' && y=='T')
		return 1;
	if(x=='T' && y=='W')
		return 1;
	return 0;
}

/*
int isMatching(char x, char y){
        if(x=='C' && y=='G')
                return 1;
        if(x=='G' && y=='C')
                return 1;
        if(x=='A' && y=='U')
                return 1;
        if(x=='U' && y=='A')
                return 1;
        return 0;
}
*/

int findMaxMatch(char* a, struct ArrayList r, int i, int j, int tmin, int tmax){
	if (tmin == tmax){
		int value;
		int t = tmin; //For sanity's sake
		if(t==i)
			value = *getPos(t+1,j-1,r) ;
		else
			value =*getPos(i,t-1,r) + *getPos(t+1,j-1,r);
		if(isMatching(a[t],a[j]))
			value++;
		return value;
	}else{
		int mid = (tmin + tmax)/2;
		int x = findMaxMatch(a,r,i,j,tmin,mid);
		int y = findMaxMatch(a,r,i,j,mid+1,tmax);
		return x > y ? x : y;
		
		
	}
}

int findMaxMatchSimple(char* a, struct ArrayList r, int i, int j, int x, int y){
	int t;
	int max = 0;
	for(t=i;t<j-4;t++){
		int value;
		if(t==i)
                        value = *getPos(t+1,j-1,r) ;
		else
			value = *getPos(i,t-1,r) + *getPos(t+1,j-1,r);
                if(isMatching(a[t],a[j]))
                        value++;
		if (value > max)
			max = value;
	}
	return max;
}

void opt(char* a, struct ArrayList r, int i, int j){
	if(i >= j-4){
		*getPos(i,j,r)=0;
		return;
	}
	int max = findMaxMatch(a,r,i,j,i,j-5);
	//int max = findMaxMatchSimple(a,r,i,j,i,j-4);
	if (max >= *getPos(i,j-1,r)){
		*getPos(i,j,r) = max;
	}else{
		*getPos(i,j,r) = *getPos(i,j-1,r);
	}
} 

int main(int argc, char *argv[]){
	FILE * f ;
	int size;
	struct ArrayList r;	
	f = fopen(argv[1],"r");
	if(argc == 3)
		size = atoi(argv[2])+2;
	else{	
		fseek(f, 0L, SEEK_END);
		size = ftell(f);
		rewind(f);
	}
	char a [size];
	fgets(a,size-1,f);
	a[size]='\0';
	//printf("%s| %d\n",a,size);
	
	r.array = calloc((size)*(size),sizeof(int));
	r.sizex = size-1;
	r.sizey = size-1;

	int i,j;
	for(i=0;i < size -1; i++){
		for(j=0;j < size-1-i;j++){
			opt(a,r,j,j+i);
		}
	}
	printf("%d\n",*getPos(0,size-2,r));
	free(r.array);
	return 0;
}
